//
//  TaskType.swift
//  Loyality
//
//  Created by Димон on 09.08.2020.
//  Copyright © 2020 Denis Romashov. All rights reserved.
//

import UIKit

class TaskType {
    
    static let checkList = "CHECK_LIST"
    static let checkListTp = "CHECK_LIST_TP"
    static let text = "TEXT"
    static let textTp = "TEXT_TP"
    static let image = "IMAGE"
    static let imageTp = "IMAGE_TP"
    static let textImage = "TEXT_IMAGE"
    static let textImageTp = "TEXT_IMAGE_TP"
}
