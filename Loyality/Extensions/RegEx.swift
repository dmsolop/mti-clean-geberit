//
//  RegEx.swift
//  Loyality
//
//  Created by Denis Romashov on 3/19/19.
//  Copyright © 2019 Denis Romashov. All rights reserved.
//

import Foundation

class RegEx {
    static let mail = "[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\\.)+(?:[a-zA-Z]{2,})\\b"
    static let name = "^[а-яА-ЯёЁa-zA-Z0-9а-щА-Щ ЬьЮюЯяЇїІіЄєҐґ]+$"
    static let phone = "^(380)\\d{9}"
    static let pin = "^[0-9]{4}$"
    
    static let life = "^(38063|38093|38073)\\d{7}"
    static let kyivstar = "^(38067|38068|38096|38097|38098)\\d{7}"
    static let vodafone = "^(38050|38066|38095|38099)\\d{7}"
    
    
    
}
