//
//  AppSettingsVC.swift
//  Loyality
//
//  Created by Димон on 22.07.2020.
//  Copyright © 2020 Denis Romashov. All rights reserved.
//

import UIKit

class AppSettingsVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    

      //MARK: - Routing
    static func storyboardInstance() -> AppSettingsVC? {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: AppSettingsVC.self)) as? AppSettingsVC
    }

}
