//
//  ViewProfileVC.swift
//  Loyality
//
//  Created by Димон on 20.08.2020.
//  Copyright © 2020 Denis Romashov. All rights reserved.
//

import UIKit

class ViewProfileVC: UIViewController {
    //MARK: - Properties
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var userImage: UIImageView!

    var message: ChatEntry?
    
    //MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.setNavigationBarHidden(false, animated: false)
        if let message = self.message, let phone = Session.phone {
            Communicator.getUserInfo(phone: phone, userID: message.toUserId) { [weak self] (response) in
                print(response)
                if let phone = response["USER_ID"] as? String {
                    self?.phoneLabel.text = "+" + phone
                }
                let url = URL(string: response["PHOTO"] as! String)
                self?.nameLabel.text = response["NAME"] as? String
                self?.emailLabel.text = response["EMAIL"] as? String
                self?.userImage.sd_setImage(with: url, completed: { (image, error, imageCash, url) in })
            }
        }
    }
    
    //MARK: - Transition
    static func storyboardInstance() -> ViewProfileVC? {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: ViewProfileVC.self)) as? ViewProfileVC
    }

}
