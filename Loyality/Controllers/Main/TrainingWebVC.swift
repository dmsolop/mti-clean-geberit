//
//  TrainingWebVC.swift
//  Loyality
//
//  Created by Димон on 21.07.2020.
//  Copyright © 2020 Denis Romashov. All rights reserved.
//

import UIKit
import WebKit

class TrainingWebVC: BaseViewController {

    //MARK: - Properties
    @IBOutlet weak var balanceLabel: UILabel!
    @IBOutlet weak var webView: WKWebView!
    
    private var stringUrl: String = ""
    private var token: String = ""
    private var userID: String = ""
    
    //MARK: - View lifecycle
    override func loadView() {
        super.loadView()
        webView.navigationDelegate = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showLoader()
        balanceLabel.text = "₴ \(Session.balance)"
        webView.allowsBackForwardNavigationGestures = true
        stringUrl = APIConfigs.trainingLink
        if let token = Session.crypt, let userID = Session.phone {
            self.token = token
            self.userID = userID
        }
        sendRequest()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    //MARK: - Request
    private func sendRequest() {
        let paramsString = ["userID": userID, "token": token]
        if let json = try? JSONSerialization.data(withJSONObject: paramsString, options: []) {
            if let content = String(data: json, encoding: .utf8) {
                let paramsBytes: [UInt8] = Array(content.utf8)
                let paramsBase64 = paramsBytes.toBase64()
                let params = ["lg": paramsBase64]
                let postString = self.getPostString(params: params as! [String : String])
                if let url = URL(string: stringUrl + "?" + postString) {
                    var urlRequest = URLRequest(url: url)
                    urlRequest.httpMethod = "GET"
                    webView.load(urlRequest)
                }
            }
        }
    }
    
    func getPostString(params:[String:String]) -> String {
        var data = [String]()
        for (key, value) in params {
            data.append(key + "=\(value)")
        }
        return data.map { String($0) }.joined(separator: "&")
    }
    
    
    //MARK: - Actions
    @IBAction func menuBtnClicked(_ sender: UIButton) {
        if let popUPMenuVC = PopUpMenuVC.storyboardInstance() {
            popUPMenuVC.modalTransitionStyle = .coverVertical
            popUPMenuVC.modalPresentationStyle = .overCurrentContext
            popUPMenuVC.delegate = self
            self.present(popUPMenuVC, animated: true, completion: nil)
        }
    }
}

//MARK: - PopUpMenu Delegate
extension TrainingWebVC: PopUpMenuDelegate {
    func selectTabBarItem(index: Int) {
        tabBarController!.selectedIndex = index
    }
    
    func sendingCurrentViewController(vc: UIViewController) {
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK: - WKNavigationDelegate
extension TrainingWebVC: WKNavigationDelegate {
    func webView(_ webView: WKWebView,didFinish navigation: WKNavigation!) { print("loaded")
        hideLoader()
    }
}
