//
//  CodeViewController.swift
//  Loyality
//
//  Created by Denis Romashov on 4/9/19.
//  Copyright © 2019 Denis Romashov. All rights reserved.
//

import UIKit
import SDWebImage
import TGLStackedViewController
import EAN13BarcodeGenerator
import RSBarcodes_Swift
import AVFoundation

class CodeViewController: TGLStackedViewController {
    @IBOutlet weak var textBarcodeLabel: UILabel!
    @IBOutlet weak var constreintTopBarcode: NSLayoutConstraint!
    @IBOutlet weak var constreintBottomBarcode: NSLayoutConstraint!
    
    var partner: Partner!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        stackedLayout?.isFillingHeight = false
        stackedLayout?.isCenteringSingleItem = true
        stackedLayout?.itemSize = CGSize(width: UIScreen.main.bounds.width, height: 240)
        exposedItemSize = CGSize(width: UIScreen.main.bounds.width, height: 320)
        
        exposedPinningMode = .all
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return partner.barcodes.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let barcode = partner.barcodes[indexPath.row]
        
        let identifier = String(describing: CardCollectionViewCell.self)
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as! CardCollectionViewCell
        cell.cardView.backgroundColor = partner.circleColor()
        cell.logoImageView.sd_setImage(with: URL(string: partner.circleLogo), completed: nil)
//        cell.barcodeView.barCode = barcode.barCode
         
        cell.amount.text = "\(barcode.balance)"
//        cell.barcodeView.shouldShowNumbers = false
        cell.barCodeNumbers.text = String(barcode.barCode)
        
        let isValidEAN13 = RSUnifiedCodeValidator.shared.isValid(barcode.barCode, machineReadableCodeObjectType: AVMetadataObject.ObjectType.ean13.rawValue)
                
        if isValidEAN13 {
            cell.barcodeImage.image = RSUnifiedCodeGenerator.shared.generateCode(barcode.barCode, machineReadableCodeObjectType: AVMetadataObject.ObjectType.ean13.rawValue)
        } else {
            cell.barcodeImage.image = RSCode128Generator(codeTable: .auto).generateCode(barcode.barCode, machineReadableCodeObjectType: AVMetadataObject.ObjectType.code128.rawValue)
        }

        return cell
    }
}
