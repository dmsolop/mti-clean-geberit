//
//  UserPayments.swift
//  Loyality
//
//  Created by Димон on 15.07.2020.
//  Copyright © 2020 Denis Romashov. All rights reserved.
//

import Foundation
import ObjectMapper

struct UserPayment: Mappable {

    var id: Int = 0
    var submitDate: String = ""
    var payDate: String = ""
    var amount: Int = 0
    var amountBefore: Int = 0
    var amountAfter: Int = 0
    var messageText: String = ""
    var answer1: String? = nil
    var answer2: String? = nil
    var codecID: String = ""
    var isShowed: Bool = false
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        id <- map["id"]
        submitDate <- map["submitDate"]
        payDate <- map["payDate"]
        amount <- map["amount"]
        amountBefore <- map["amountBefore"]
        amountAfter <- map["amountAfter"]
        messageText <- map["messageText"]
        answer1 <- map["answer1"]
        answer2 <- map["answer2"]
        codecID <- map["codecID"]
    }
}
