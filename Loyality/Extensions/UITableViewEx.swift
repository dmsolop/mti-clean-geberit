//
//  UITableViewEx.swift
//  Loyality
//
//  Created by Dima on 9/2/19.
//  Copyright © 2019 Denis Romashov. All rights reserved.
//

import Foundation
import UIKit

extension UITableView {
    
    func scrollToCurrent(_ indexPath: IndexPath, completion: @escaping ()->()) {
        DispatchQueue.main.async {
            self.scrollToRow(at: indexPath, at: .bottom, animated: true)
            completion()
        }
    }
    
    func scrollToBottom(){

        DispatchQueue.main.async {
            let indexPath = IndexPath(
                row: self.numberOfRows(inSection:  self.numberOfSections-1) - 1,
                section: self.numberOfSections - 1)
            self.scrollToRow(at: indexPath, at: .bottom, animated: true)
        }
    }

    func scrollToTop() {

        DispatchQueue.main.async {
            let indexPath = IndexPath(row: 0, section: 0)
            self.scrollToRow(at: indexPath, at: .top, animated: false)
        }
    }
}


