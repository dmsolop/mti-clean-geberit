//
//  ServerError.swift
//  Skeleton
//
//  Created by Woxapp on 21.11.17.
//  Copyright © 2017 Woxapp. All rights reserved.
//

import UIKit

class ServerError: NSObject {

    private static var applicationName: String? {
        return Bundle.main.infoDictionary![kCFBundleNameKey as String] as? String
    }

    private class var isShowing: Bool {
        return UIApplication.topViewController() is UIAlertController
    }

    private static var dictionary: [Int: String] {
        var dict: [Int: String] = [:]
        dict[777] = NSLocalizedString("Пожалуйста, проверьте подключение к сети.", comment: "")
        dict[778] = NSLocalizedString("Неверный формат имени.", comment: "")
        dict[779] = NSLocalizedString("Неверный формат телефона.", comment: "")
        dict[780] = NSLocalizedString("Неверный формат email.", comment: "")
        dict[781] = NSLocalizedString("Неверный формат ссылки.", comment: "")
        dict[782] = NSLocalizedString("Необходимо заполнить все поля.", comment: "")
        dict[783] = NSLocalizedString("Код отправлен Вам в сообщении.", comment: "")
        dict[785] = NSLocalizedString("Пароль успешно изменен.", comment: "")
        dict[406] = NSLocalizedString("Номер уже зарегистрирован", comment: "")
        dict[1000] = "Server Error"
        dict[401] = NSLocalizedString("Время безопасного сеанса истекло. Авторизируйтсь, пожалуйста, снова", comment: "")
        return dict
    }

    class func errorText(id: Int) -> String? {
        return dictionary[id]
    }

    class func showError(_ id: Int) {
        if let message = dictionary[id] {
            show(alert: message)
        }
    }

    class func show(alert message: String, _ title: String? = nil, _ button: String? = nil, _ handler: ((UIAlertAction) -> Void)? = nil) {
        if isShowing == true {
            return
        }
        let alert = UIAlertController(title: title ?? applicationName, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: button ?? "OK", style: .default, handler: handler))
        UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
    }

    class func showTouchIDAlert() {
        let alert = UIAlertController(title: applicationName, message: "CurrentDevice.capability.explanation", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("Отмена", comment: ""), style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: NSLocalizedString("Настроить", comment: ""), style: .default, handler: { _ in
            self.openSettings()
        }))
        UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
    }
    
    private class func openSettings() {
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString)
            else {
                return
        }
        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.openURL(settingsUrl)
        }
    }

}
