//
//  MTICipher.h
//  Loyality
//
//  Created by Denis Romashov on 3/22/19.
//  Copyright © 2019 Denis Romashov. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import <RNCryptor/RNCryptor.h>

NS_ASSUME_NONNULL_BEGIN

@interface MTICipher : NSObject

//+ (NSData *)doCipher:(NSData *)dataIn
//                 key:(NSData *)symmetricKey
//             context:(CCOperation)encryptOrDecrypt; // kCCEncrypt or kCCDecrypt
//
//+ (NSData *)encryptDataWithAESECB:(NSData *)data
//                              key:(NSData *) key
//                            error:(NSError **)error;
+ (NSData *)encrypt:(NSString *)message key:(NSData *)key;

@end



NS_ASSUME_NONNULL_END


