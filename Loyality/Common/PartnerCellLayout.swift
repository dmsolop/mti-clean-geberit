//
//

import UIKit

enum PartnerViewStyle: Int {
    case tileStyle = 0
    case tableStyle
    
    public var toString: String {
        switch self {
        case .tableStyle:
            return "TABLE"//.localized
        case .tileStyle:
            return "GRID"//.localized
        }
    }
    
    static let allValues = [tileStyle.toString, tableStyle.toString]
}

protocol PartnerCellLayoutDelegate: class {
    func collectionView(_ collectionView: UICollectionView) -> PartnerViewStyle
}

final class PartnerCellLayout: UICollectionViewLayout {
    
    weak var delegate: PartnerCellLayoutDelegate?
    
    private var numberOfColumns: Int {
        get {
            switch self.collectionViewStyle {
            case .tableStyle?:
                return 1
            default:
                return 2
            }
        }
    }
    
    private var cellPadding: CGFloat = 3.5
    private var collectionViewStyle: PartnerViewStyle!
    
    private var cache = [UICollectionViewLayoutAttributes]()
    
    private var contentHeight: CGFloat = 0.0
    private var cellHeight: CGFloat {
        get {
            switch self.collectionViewStyle {
            case .tableStyle?:
                return 200.0
            default:
                return 200.0
            }
        }
    }
    private var contentWidth: CGFloat {
        guard let collectionView = collectionView else {
            return 0
        }
        let insets = collectionView.contentInset
        return collectionView.bounds.width - (insets.left + insets.right)
    }
    
    override var collectionViewContentSize: CGSize {
        return CGSize(width: contentWidth, height: contentHeight)
    }
    
    override func prepare() {
        guard let collectionView = collectionView else {
            return
        }
        
//        if self.collectionViewStyle != self.delegate.collectionView(collectionView) || cache.count != collectionView.numberOfItems(inSection: 0) {
            self.contentHeight = 0.0
            self.cache.removeAll()
//        }
        
//        guard cache.isEmpty == true else {
//            return
//        }
        
        self.collectionViewStyle = self.delegate?.collectionView(collectionView)
        let columnWidth = contentWidth / CGFloat(numberOfColumns)
        var xOffset = [CGFloat]()
        for column in 0 ..< numberOfColumns {
            xOffset.append(CGFloat(column) * columnWidth)
        }
        var column = 0
        var yOffset = [CGFloat](repeating: 0, count: numberOfColumns)
        
        for item in 0 ..< collectionView.numberOfItems(inSection: 0) {
            
            let indexPath = IndexPath(item: item, section: 0)
            
            let height = cellPadding * 2 + self.cellHeight
            let frame = CGRect(x: xOffset[column], y: yOffset[column], width: columnWidth, height: height)
            let insetFrame = frame.insetBy(dx: cellPadding, dy: cellPadding)
            
            let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
            attributes.frame = insetFrame
            cache.append(attributes)
            
            self.contentHeight = max(self.contentHeight, frame.maxY)
            yOffset[column] = yOffset[column] + height
            
            column = column < (numberOfColumns - 1) ? (column + 1) : 0
        }
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var visibleLayoutAttributes = [UICollectionViewLayoutAttributes]()
        
        for attributes in cache {
            if attributes.frame.intersects(rect) {
                visibleLayoutAttributes.append(attributes)
            }
        }
        return visibleLayoutAttributes
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return cache[indexPath.item]
    }
}
