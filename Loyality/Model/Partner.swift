//
//  Partner.swift
//  Loyality
//
//  Created by Denis Romashov on 4/6/19.
//  Copyright © 2019 Denis Romashov. All rights reserved.
//

import Foundation
import ObjectMapper

struct Partner: Mappable {
    
    var id: Int = 0
    var balance: Int = 0
    var name: String = ""
    var logo: String = ""
    var logoBig: String = ""
    var colorHex: String = ""
    
    var circleLogo: String = ""
    var circleLogoBig: String = ""
    var circleColorHex: String = ""
    
    var barcodes = [Barcode]()
    var nominal = [Int]()
    
    var active = -1
    var isDiscrete = false
    var isMobileOperator = false
    
    func color() -> UIColor {
        return UIColor(hex: colorHex)
    }
    
    func circleColor() -> UIColor {
        return UIColor(hex: circleColorHex)
    }
    
    init?(map: Map) { }
    
    mutating func mapping(map: Map) {
        id <- map["PARTNER_ID"]
        name <- map["PARTNER_NAME"]
        logo <- map["LOGO"]
        logoBig <- map["BIG_LOGO"]
        colorHex <- map["LOGO_COLOR"]
        circleLogo <- map["DISC_LOGO"]
        circleLogoBig <- map["BIG_DISC_LOGO"]
        circleColorHex <- map["DISC_COLOR"]
        active <- map["ACTIVE"]
        isDiscrete <- map["DISCRETE"]
        isMobileOperator <- map["MOBILE_OPERATOR"]
        balance <- map["BALANCE"]
        nominal <- map["NOMINAL"]
        let json = map.JSON["PARTNER_BAR_CODES"] as? [[String: Any]] ?? []
        json.forEach { (item) in
            if let barcode = Mapper<Barcode>().map(JSON: item) {
                if barcode.barCode.isEmpty == false {
                    barcodes.append(barcode)
                }
            }
        }
    }
}
